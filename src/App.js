import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import MenuBar from './components/MenuBar';
import Author from './pages/Author';



export default function App() {
  return (
    <Router>
      <MenuBar />
      <Switch>
        <Route path='/author' component={Author} />
      </Switch>
    </Router>
  )
}
